import faker from 'faker/locale/es_MX'

export function getAll (state, payload) {
  return new Promise((resolve, reject) => {
    let data = []
    for (let i = 0; i < 30; i++) {
      data.push({
        id: faker.random.uuid(),
        title: faker.name.title(),
        description: faker.lorem.words(10),
        rating: faker.random.number(1, 3),
        image: [
          faker.image.imageUrl(null, null, 'cats')
          /*,
          '?nc=',
          new Date().getTime()
          */
        ].join('')
      })
    }
    setTimeout(() => {
      state.commit('setRecords', data)
      resolve()
    }, 600)
  })
}
